<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
//login
// Route::get('/login', ['as' => 'login', 'uses' => 'AuthController@index']);
// Route::post('/postlogin', ['as' => 'post_login', 'uses' => 'AuthController@authenticate']);
// Route::get('/logout', ['as' => 'logout', 'uses' => 'AuthController@logout']);
// // middleware

Route::get('/', ['as' => 'home', 'uses' => 'HomeController@index']);
Route::get('book', ['as' => 'book.list', 'uses' => 'BookController@index']);
Route::post('book', ['as' => 'book.add', 'uses' => 'BookController@add']);

Route::get('docgia', ['as' => 'docgia.list', 'uses' => 'DocgiaController@index']);
Route::post('docgia', ['as' => 'docgia.add', 'uses' => 'DocgiaController@add']);

Route::get('muondoc', ['as' => 'muondoc.list', 'uses' => 'UserBookController@index']);
Route::post('muondoc', ['as' => 'muondoc.add', 'uses' => 'UserBookController@add']);

