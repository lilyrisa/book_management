@extends('layout.main')

@section('main')
@include('layout.page-title',['name' => 'Khách đoc', 'tree' => 2])

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Data Export</h4>
                <button type="button" style="margin-bottom: 10px" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                    Thêm Khách đoc
                  </button>
                  <!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Thêm sách</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="form-group">
              <label for="">Tên sách</label>
                <select id="id_book" class="form-control">
                    @foreach($book as $b)
                    <option value="{{$b->id}}">{{$b->tensach}}</option>
                    @endforeach
                </select>
          </div>
          <div class="form-group">
            <label for="">Nhà sản xuất</label>
            <select id="id_dongia" class="form-control">
                @foreach($dongia as $d)
                <option value="{{$d->id}}">{{$d->tendocgia}}</option>
                @endforeach
            </select>
      </div>
      <div class="form-group">
        <label for="">Thời gian mượn</label>
        <input type="date" class="form-control" id="timestart">
  </div>
  <div class="form-group">
    <label for="">Thời gian trả</label>
    <input type="date" class="form-control" id="timeend">
</div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary" id="sbbook">Save changes</button>
        </div>
      </div>
    </div>
  </div>
                <h6 class="card-subtitle">Export data to Copy, CSV, Excel, PDF & Print</h6>
                <div class="table-responsive m-t-40">
                    <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>STT</th>
                                <th>Tên sách</th>
                                <th>Tên độc giả</th>
                                <th>thời gian mượn</th>
                                <th>Thời gian trả</th>
                                <th>Gía tiền tạm tính</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($userbook as $u_b)
                            <tr>
                                <td>{{$u_b->id}}</td>
                                @foreach($book as $bb)
                                    @if($bb->id == $u_b->id_book)
                                        <td>{{$bb->tensach}} ({{$bb->costtime }} vnd/day)</td>
                                    @endif
                                @endforeach
                                
                                @foreach($dongia as $dg)
                                @if($dg->id == $u_b->id_dongia)
                                <td>{{$dg->tendocgia}}</td>
                                @endif
                                @endforeach
                                {{-- <td>{{$u_b->dongia->tendocgia}}</td> --}}
                                <td>{{$u_b->timestart}}</td>
                                <td>{{$u_b->timeend}}</td>
                                <td>{{$u_b->cost}} vnd</td>
                            </tr>
                          @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection



@section('script')
@include('layout.script')
<script>
    $(function () {
        $('#sbbook').on('click', function(){
            var data = {
                id_book: $('#id_book').val(),
                id_dongia: $('#id_dongia').val(),
                timestart: $('#timestart').val(),
                timeend: $('#timeend').val(),
            }
            $.ajax({
                url:'{{route("muondoc.add")}}',
                type:'post',
                data:{
                    ...data,
                    '_token': '{{csrf_token()}}'
                }
            })
            .done(res=>{
                if(res.is){
                    $.MessageBox({
                        buttonDone: "OK",
                        buttonFail : undefined,
                        top: "25%",
                        input: false,
                        message: "Thêm thành công",
                        queue: true,
                        speed: 200,
                    });
                    setTimeout(()=>{location.reload()},2000)
                }else{
                    $.MessageBox({
                        buttonDone: "OK",
                        buttonFail : undefined,
                        top: "25%",
                        input: false,
                        message: "Lôi",
                        queue: true,
                        speed: 200,
                    });
                }
            })
        })
        $('#myTable').DataTable();
        var table = $('#example').DataTable({
            "columnDefs": [{
                "visible": false,
                "targets": 2
            }],
            "order": [
                [2, 'asc']
            ],
            "displayLength": 25,
            "drawCallback": function (settings) {
                var api = this.api();
                var rows = api.rows({
                    page: 'current'
                }).nodes();
                var last = null;
                api.column(2, {
                    page: 'current'
                }).data().each(function (group, i) {
                    if (last !== group) {
                        $(rows).eq(i).before('<tr class="group"><td colspan="5">' + group + '</td></tr>');
                        last = group;
                    }
                });
            }
        });
        // Order by the grouping
        $('#example tbody').on('click', 'tr.group', function () {
            var currentOrder = table.order()[0];
            if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
                table.order([2, 'desc']).draw();
            } else {
                table.order([2, 'asc']).draw();
            }
        });
        // responsive table
        $('#config-table').DataTable({
            responsive: true
        });
        $('#example23').DataTable({
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ]
        });
        $('.buttons-copy, .buttons-csv, .buttons-print, .buttons-pdf, .buttons-excel').addClass('btn btn-primary mr-1');
    });

</script>
@endsection