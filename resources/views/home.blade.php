@extends('layout.main')

@section('name')
    <style>
    #myVideo {
  position: fixed;
  right: 0;
  bottom: 0;
  min-width: 100%; 
  min-height: 100%;
}

.content {
  position: fixed;
  bottom: 0;
  background: rgba(0, 0, 0, 0.5);
  color: #f1f1f1;
  width: 100%;
  padding: 20px;
}


    </style>
@endsection
@section('main')

<video autoplay muted loop id="myVideo">
    <source src="{{asset('images/backg.mp4')}}" type="video/mp4">
    Your browser does not support HTML5 video.
  </video>

@endsection



@section('script')
@include('layout.script')
@endsection