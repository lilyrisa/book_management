@extends('layout.main')

@section('main')
@include('layout.page-title',['name' => 'Quản lý sách', 'tree' => 2])

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Data Export</h4>
                <button type="button" style="margin-bottom: 10px" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                    Thêm sách
                  </button>
                  <!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Thêm sách</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="form-group">
              <label for="">Tên sách</label>
                <input type="text" class="form-control" id="tensach">
          </div>
          <div class="form-group">
            <label for="">Nhà sản xuất</label>
            <input type="text" class="form-control" id="nsx">
      </div>
      <div class="form-group">
        <label for="">Ghi chú</label>
        <input type="text" class="form-control" id="ghichu">
  </div>
  <div class="form-group">
    <label for="">Gía tiền mượn theo thời gian</label>
    <input type="number" class="form-control" id="costtime">
</div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary" id="sbbook">Save changes</button>
        </div>
      </div>
    </div>
  </div>
                <h6 class="card-subtitle">Export data to Copy, CSV, Excel, PDF & Print</h6>
                <div class="table-responsive m-t-40">
                    <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Mã sách</th>
                                <th>Tên sách</th>
                                <th>Nhà sản xuất</th>
                                <th>Ghi chú</th>
                                <th>Gía theo thời gian</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($book as $b_i)
                            <tr>
                                <td>{{$b_i->id}}</td>
                                <td>{{$b_i->tensach}}</td>
                                <td>{{$b_i->nsx}}</td>
                                <td>{{$b_i->ghichu}}</td>
                                <td>{{$b_i->costtime}}</td>
                            </tr>
                          @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection



@section('script')
@include('layout.script')
<script>
    $(function () {
        $('#sbbook').on('click', function(){
            var data = {
                tensach: $('#tensach').val(),
                nsx: $('#nsx').val(),
                ghichu: $('#ghichu').val(),
                costtime: $('#costtime').val(),
            }
            $.ajax({
                url:'{{route("book.add")}}',
                type:'post',
                data:{
                    ...data,
                    '_token': '{{csrf_token()}}'
                }
            })
            .done(res=>{
                if(res.is){
                    $.MessageBox({
                        buttonDone: "OK",
                        buttonFail : undefined,
                        top: "25%",
                        input: false,
                        message: "Thêm thành công",
                        queue: true,
                        speed: 200,
                    });
                    setTimeout(()=>{location.reload()},2000)
                }else{
                    $.MessageBox({
                        buttonDone: "OK",
                        buttonFail : undefined,
                        top: "25%",
                        input: false,
                        message: "Lôi",
                        queue: true,
                        speed: 200,
                    });
                }
            })
        })
        $('#myTable').DataTable();
        var table = $('#example').DataTable({
            "columnDefs": [{
                "visible": false,
                "targets": 2
            }],
            "order": [
                [2, 'asc']
            ],
            "displayLength": 25,
            "drawCallback": function (settings) {
                var api = this.api();
                var rows = api.rows({
                    page: 'current'
                }).nodes();
                var last = null;
                api.column(2, {
                    page: 'current'
                }).data().each(function (group, i) {
                    if (last !== group) {
                        $(rows).eq(i).before('<tr class="group"><td colspan="5">' + group + '</td></tr>');
                        last = group;
                    }
                });
            }
        });
        // Order by the grouping
        $('#example tbody').on('click', 'tr.group', function () {
            var currentOrder = table.order()[0];
            if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
                table.order([2, 'desc']).draw();
            } else {
                table.order([2, 'asc']).draw();
            }
        });
        // responsive table
        $('#config-table').DataTable({
            responsive: true
        });
        $('#example23').DataTable({
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ]
        });
        $('.buttons-copy, .buttons-csv, .buttons-print, .buttons-pdf, .buttons-excel').addClass('btn btn-primary mr-1');
    });

</script>
@endsection