<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Book;

class BookController extends Controller
{
    public function index(){
        $book = Book::all();
        return view('book',['book'=>$book]);
    }
    public function add(Request $request){
        $book = new Book();
        $book->fill($request->all());
        $book->save();
        return response()->json(['is'=>true]);
    }
}
