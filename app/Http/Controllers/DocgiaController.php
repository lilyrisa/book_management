<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\dongia;

class DocgiaController extends Controller
{
    public function index(){
        $dongia = dongia::all();
        return view('docgia',['dongia'=>$dongia]);
    }
    public function add(Request $request){
        $dongia = new dongia();
        $dongia->fill($request->all());
        $dongia->save();
        return response()->json(['is'=>true]);
    }
}
