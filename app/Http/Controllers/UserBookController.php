<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Book;
use App\Models\dongia;
use App\Models\UserBook;
use DateTime;

class UserBookController extends Controller
{
    public function index(){
        $userbook = UserBook::all();
        $book = Book::all();
        $dongia = dongia::all();
        for($i=0; $i< count($userbook); $i++){
            for($j=0; $j< count($book); $j++){
                if($userbook[$i]->id_book == $book[$j]->id){
                    $earlier = new DateTime($userbook[$i]->timestart);
                    $later = new DateTime($userbook[$i]->timeend);   
                    $diff = $later->diff($earlier)->format("%a");
                    $userbook[$i]->cost = $diff*$book[$j]->costtime;
                }
            }
            
        }
        return view('userbook',['userbook'=>$userbook, 'book' => $book, 'dongia' => $dongia]);
    }
    public function add(Request $request){
        $userbook = new UserBook();
        $userbook->fill($request->all());
        $userbook->save();
        return response()->json(['is'=>true]);
    }
}
