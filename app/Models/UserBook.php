<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserBook extends Model
{
    use HasFactory;
    protected $fillable = [
        'id_book',
        'id_dongia',
        'timestart',
        'timeend',
    ];

    public function Book() {
        return $this->belongsTo('Book');
    }
    public function dongia() {
        return $this->belongsTo('dongia');
    }
}
