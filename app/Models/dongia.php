<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class dongia extends Model
{
    use HasFactory;

    protected $fillable = [
        'tendocgia',
        'sdt',
        'dc',
        'ghichu',
    ];

    public function UserBook() {
        return $this->hasMany('UserBook');
    }
}
