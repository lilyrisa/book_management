<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    use HasFactory;

    protected $fillable = [
        'tensach',
        'nsx',
        'ghichu',
        'costtime',
    ];
    public function UserBook() {
        return $this->hasMany('UserBook');
    }
}
